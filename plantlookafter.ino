// C++ code
//
// Include necessary libraries
#include <ThingSpeak.h>
#include <WiFiNINA.h>
#include <OneWire.h>
#include <DallasTemperature.h>

// WiFi credentials
char ssid[] = "TALKTALK36ABAE";
char pass[] = "7QMEJFHE";

// ThingSpeak channel and API key for soil moisture
unsigned long SoilCON = 2394379;
const char *apiKey1 = "MUTSVSOLZL6KL8DA";

// ThingSpeak channel and API key for air quality
unsigned long AirCON = 2396379;  
const char *apiKey2 = "U710RYD1WP7VG176";  

// ThingSpeak channel and API key for temperature
unsigned long TempCON = 2396380;
const char *apiKey3 = "7XN5R7O9MP2FXYHN";

// ThingSpeak channel and API key for light intensity
unsigned long LightCON = 2396382;
const char *apiKey4 = "QHFFHW256CFY4CN4";

WiFiClient client;

// Calibration and timing variables for motion sensor
int calibrationTime = 10; 
long unsigned int lowIn; 
long unsigned int pause = 5000;

boolean lockLow = true;
boolean takeLowTime; 

//For temperature sensor
float temp = 0.0;
int oneWireBus = 12;
OneWire oneWire(oneWireBus);
DallasTemperature sensors(&oneWire);

// Pin configurations for PIR sensor, buzzer, and LEDs
int pirPin = 4; 
int buzzPin = 13;

int sensor;
const int powerpin = 8;
const int delayTime = 1000;
const int redLED = 2;
const int greenLED = 3;
const int thresh = 500;

boolean motionDetectionEnabled = true;

// Setup function
void setup() {
  // Start serial communication
  Serial.begin(9600);
  
  // Connect to WiFi
  if (WiFi.begin(ssid, pass) != WL_CONNECTED) {
      Serial.println("Failed to connect to WiFi");
      while (1) {
        delay(5000);
      }
  }

  // Initialize ThingSpeak
  ThingSpeak.begin(client);

  // Set up pin modes for various components
  pinMode(powerpin, OUTPUT);
  pinMode(redLED, OUTPUT);
  pinMode(greenLED, OUTPUT);
  pinMode(pirPin, INPUT);
  pinMode(buzzPin, OUTPUT);
  digitalWrite(pirPin, LOW);
  
  // Calibrate motion sensor
  Serial.print("calibrating sensor ");
  for(int i = 0; i < calibrationTime; i++) {
    Serial.print(".");
    delay(1000);
  }
  Serial.println(" done");
  Serial.println("SENSOR ACTIVE");
  delay(50);

  // Initialize temperature sensor
  Serial.println("Bas on Tech - 1-wire temperature sensor");
  sensors.begin();
}

// Main loop
void loop() {
  // Read soil moisture sensor
  digitalWrite(powerpin, HIGH);
  delay(10);
  sensor = analogRead(A0);
  digitalWrite(powerpin, LOW);
  Serial.print("Moisture Level= ");
  Serial.println(sensor);
  
  // Send soil moisture data to ThingSpeak
  ThingSpeak.writeField(SoilCON, 1, sensor, apiKey1);

  // Check soil moisture level and control LEDs accordingly
  if (sensor > thresh) {
    digitalWrite(redLED, LOW);
    digitalWrite(greenLED, HIGH);
  } else {
    digitalWrite(redLED, HIGH);
    digitalWrite(greenLED, LOW);
  }

  // Check for motion using PIR sensor
  if (digitalRead(pirPin) == HIGH) {
    // Activate buzzer with different tones
    tone(buzzPin, 3000, 2000);
    delay(100);
    tone(buzzPin, 2000, 1000);
    delay(100);
    tone(buzzPin, 3000, 2000);
    delay(100);
    tone(buzzPin, 1000, 1000);
    delay(100);
    
    // If motion is detected, print time and loaction
    if (lockLow) {
      lockLow = false;            
      Serial.println("---");
      Serial.print("motion detected at ");
      Serial.print(millis()/1000);
      Serial.println(" sec"); 
      delay(50);
    }         
    takeLowTime = true;
  }

  // Read air quality sensor
  int AirValue = analogRead(A1);
  Serial.print("AirQuality=");
  Serial.print(AirValue, DEC);
  Serial.print(" PPM");
  Serial.println();
  
  // Send air quality data to ThingSpeak
  ThingSpeak.writeField(AirCON, 1, AirValue, apiKey2);
  delay(100);

  // Read temperature sensor
  sensors.requestTemperatures();
  temp = sensors.getTempCByIndex(0);
  Serial.print("Temperature is: ");
  Serial.println(temp);
  
  // Send temperature data to ThingSpeak
  ThingSpeak.writeField(TempCON, 1, temp, apiKey3);
  delay(100);

  // Read light intensity sensor
  int SensorLight = analogRead(A5);
  float voltage = SensorLight * (5.0 / 1023.0);
  Serial.print("UV Index = ");
  Serial.println(voltage/.1);
  float UvValue = voltage / 0.1;

  // Send light intensity data to ThingSpeak
  ThingSpeak.writeField(LightCON, 1, UvValue, apiKey4);
  delay(100);

  // Check for the end of motion
  if (digitalRead(pirPin) == LOW) {
    digitalWrite(buzzPin, LOW);
    if (takeLowTime) {
      lowIn = millis();          
      takeLowTime = false;      
    }
    
    // If motion ended, print time and location
    if (!lockLow && millis() - lowIn > pause) { 
      lockLow = true;                        
      Serial.print("motion ended at ");     
      Serial.print((millis() - pause)/1000);
      Serial.println(" sec");
      delay(50);
    }
  }
  
  // Print separator for better readability
  Serial.println("----------------------------"); 
  delay(delayTime);
}
